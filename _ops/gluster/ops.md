# glusterfs 集群运维记录

## 集群出现节点故障

- [参考1](http://www.mamicode.com/info-detail-880732.html)
- [参考2](https://blog.csdn.net/weiyuefei/article/details/79022651)

### 查看失联节点

```bash
# 查看集群对应 pod 的状态, 发现 glusterfs-storage-5nb8j 已失联
oc get pod
'
NAME                                          READY     STATUS    RESTARTS   AGE
glusterblock-storage-provisioner-dc-1-9sdg2   1/1       Running   0          14d
glusterfs-storage-5nb8j                       0/1       Running   0          18m
glusterfs-storage-mwkk5                       1/1       Running   0          18m
glusterfs-storage-t5bjd                       1/1       Running   0          18m
heketi-storage-1-gtbx6                        1/1       Running   4          14d
'

# 进入失联容器
oc rsh glusterfs-storage-5nb8j bash

# 查看守护进程状态, 发现守护进程无法正常启动且无日志
systemctl status glusterd
```

### 修复失联节点

```bash
# 进入失联容器
oc rsh glusterfs-storage-5nb8j bash
# 停止守护进程
systemctl stop glusterd
# 清除掉所有已有 gluster 配置
rm -rf /var/lib/glusterd/* /var/log/glusterfs/* /etc/glusterfs/* /etc/glusterfs_bkp/*

# 进入正常 gluster 节点
oc rsh glusterfs-storage-mwkk5 bash
# 获取失联 (Disconnected) 节点及 UUID
gluster peer status
'
Number of Peers: 2

Hostname: route1.oso.x
Uuid: b6f632e3-4402-4d50-a732-0e9bad392c62
State: Peer in Cluster (Connected)

Hostname: node2.oso.x
Uuid: 956a39cd-f036-4e82-965b-9865b817b1b3
State: Peer in Cluster (Disconnected)
'

# 获取未失联节点的 operating-version
cat /var/lib/glusterd/glusterd.info
'
UUID=b6f632e3-4402-4d50-a732-0e9bad392c62
operating-version=40100
'

# 配置失联节点 glusterd.info
cat > /var/lib/glusterd/glusterd.info << EOF
UUID=956a39cd-f036-4e82-965b-9865b817b1b3
operating-version=40100
EOF

# 删除失联节点应用的 pod, 让其自动修复, 等待几分钟后, gluster 集群恢复正常
oc delete pod/glusterfs-storage-5nb8j --force --grace-period=0
```