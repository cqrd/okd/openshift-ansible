# README

## 1.CentOS/RHEL (虚拟机) 基础配置

```bash
# 基于桥接模式设置固定 ip
cat >> /etc/sysconfig/network-scripts/ifcfg-enp0s3 << EOF
# network setting
BOOTPROTO=static
IPADDR=192.168.1.61
GATEWAY=192.168.1.1
DNS1=114.114.114.114
DNS2=8.8.8.8
EOF
service network restart

# 切换默认的 yum 源
ansible oso -u root -i _ops/hosts -m shell -a "mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.bak"
ansible oso -u root -i _ops/hosts -m shell -a "curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo"
ansible oso -u root -i _ops/hosts -m shell -a "yum update -y"
```

## 2.Origin 部署及卸载

```bash
# 获取 ansible 脚本
git clone --depth=1 -b release-3.11 https://github.com/openshift/openshift-ansible openshift-ansible-3.11

# 定义个性化配置
mkdir -p _ops
```

- [部署 okd 集群](origin/README.md)
